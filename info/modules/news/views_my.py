# 自己创建到视图文件Py
from flask import abort, jsonify
from flask import current_app
from flask import g
from flask import request
from flask import session

from info import constants, db
from info.models import News, User, Comment, CommentLike
from info.modules.news import news_blu
from flask import render_template

from info.utils.common import user_login_data
from info.utils.response_code import RET


@news_blu.route('/followed_user', methods = ["POST"] )
@user_login_data
def followed_user():
    """关注/取消关注用户"""
    #0.取到自己的登录信息
    user = g.user
    if not user:
        return jsonify(errno = RET.SESSIONERR ,errmsg = "用户未登录")

    #1.取到参数
    user_id =request.json.get("user_id")
    action = request.json.get("action")

    #2.判断参数
    if not all([user_id,action]):
        return jsonify(errno = RET.PARAMERR,errmsg = "参数错误")
    if action not in ("follow","unfollow"):
        return jsonify(errno = RET.PARAMERR,errmsg = "参数错误")

    #3.查询要被关注的用户信息
    try:
        other = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return  jsonify(errno = RET.DBERR ,errmsg = "查询数据错误")
    if not other:
        return jsonify(errno = RET.NODATA,errmsg = "未查询到数据")

    #4.根据要执行的操作去修改对应的数据
    if action == "follow":
        if other not in user.followed:
            # 当前用户的关注列表中添加一个值
            user.followed.append(other)
        else:
            return jsonify(errno =RET.DATAEXIST,errmsg = "当前用户已经被关注")

    else:
        #取消关注
        if other in user.followed:
            user.followed.remove(other)
        else:
            return jsonify(errno = RET.DATAEXIST,errmsg = "当前用户未被关注")

    # 返回数据
    return jsonify(errno =RET.OK ,errmsg = "操作成功")




@news_blu.route("/comment_like",methods = ["POST"])
@user_login_data
def comment_like():
    """
    评论点赞
    :return:
    """
    user = g.user
    if not user:
        return jsonify(errno = RET.SESSIONERR,errmsg = "用户没有登录")

    #1. 取到请求的数据
    comment_id = request.json.get("comment_id")
    action =request.json.get("action")
    # news_id = request.json.get("news_id")
    # print(comment_id,action)
    if not all([comment_id,action]):
        return jsonify(errno = RET.PARAMERR ,errmsg ="参数不足或者错误")

    if action not in (["add","remove"]):
        return jsonify(errno = RET.PARAMERR ,errmsg ="参数不足或者错误")

    try:
        comment_id =int(comment_id)
        # news_id =int(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno = RET.PARAMERR ,errmsg ="参数错误")

    try:
        comment =Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return  jsonify(errno = RET.DBERR,errmsg = "数据查询错误")

    if not comment:
        return  jsonify(errno = RET.NODATA,errmsg = "评论不存在")

    if action =="add":
        comment_like_model = CommentLike.query.filter(CommentLike.user_id == user.id,
                                                      CommentLike.comment_id == comment.id).first()
        if not  comment_like_model:
        #点赞评论
            comment_like_model =CommentLike()
            comment_like_model.user_id =user.id
            comment_like_model.comment_id = comment_id
            db.session.add(comment_like_model)


    else:
        #取消点赞评论
        comment_like_model = CommentLike.query.filter(CommentLike.user_id ==user.id,CommentLike.comment_id ==comment.id).first()
        if comment_like_model:
            # comment_like_model.delete()
            db.session.delete(comment_like_model)
    return jsonify(errno = RET.OK ,errmsg = "OK")



@news_blu.route('/news_comment',methods = ['POST'])
@user_login_data
def comment_news():
    """
    评论新闻或者回复某条新闻下指定到评论
    :return:
    """

    user = g.user
    if not user:
        return jsonify(errno= RET.SESSIONERR,errmsg = "用户没有登录")

    #1.取到请求参数
    news_id  = request.json.get("news_id")
    comment_content  = request.json.get("comment")
    parent_id = request.json.get("parent_id")


    # 2. 判断参数
    if not all([news_id,comment_content]):
        return jsonify(errno = RET.PARAMERR,errmsg = "参数错误")

    try:
        news_id = int(news_id)
        if parent_id:
            parent_id = int(parent_id)
    except Exception as e:
        current_app.logger(e)
        return jsonify(errno =RET.PARAMERR,errmsg = "参数错误")

    # 3. 查询新闻并判断是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno = RET.DBERR,errmsg = "数据查询错误")

    if not news:
        return jsonify(errno = RET.NODATA,errmsg = "未查询到新闻数据")

    # 4. 初始化一个评论模型，并且赋值
    comment = Comment()
    comment.user_id = user.id
    comment.news_id = news_id
    comment.content =comment_content
    if parent_id:
        comment.parent_id = parent_id

    #5. 添加到数据库
        #为什么要自己去commit()?,因为在return到时候需要用到comment中到id
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()



    return jsonify(errno = RET.OK , errmsg = "OK", data = comment.to_dict())





@news_blu.route('/news_collect',methods =['POST'])
@user_login_data
def collect_news():
    """
    收藏新闻
    1.接收参数
    2.判断参数
    3.查询新闻，并判断新闻是否存在
    :return:
    """
    user = g.user
    if not user:
        return jsonify(errno = RET.SESSIONERR,errmsg = "用户没有登录")

    #1.接收参数
    news_id = request.json.get("news_id")
    action = request.json.get("action")

    #2.判断参数
    if not all([news_id,action]):
        return jsonify(errno = RET.PARAMERR,errmsg = "参数错误")

    #继续判断
    if action not in ["collect","cancel_collect"]:
        return jsonify(errno = RET.PARAMERR,errmsg = "参数错误")


    try:
        news_id = int(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return  jsonify(errno = RET.PARAMERR,errmsg = "参数错误")

    # 3. 查询新闻并判断是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno = RET.DBERR,errmsg = "数据查询错误")

    if not news:
        return jsonify(errno = RET.NODATA,errmsg = "未查询到新闻数据")


    if action =="cancel_collect":
        #取消收藏
        if news in user.collection_news:
            user.collection_news.remove(news)
    else:
        # 4. 收藏
        if news  not in user.collection_news:
            #添加到用户到新闻收藏列表
            user.collection_news.append(news)
    return jsonify(errno  =RET.OK, errmsg = "操作成功")



@news_blu.route('/<int:news_id>')
@user_login_data
def news_detail(news_id):
    """
    新闻详情
    :param news_id:
    :return:
    """
    # # 查询用户登录信息
    # user_id = session.get("user_id")
    # user = None
    # if user_id:
    #     # 尝试查询用户的模型
    #     try:
    #         user = User.query.get(user_id)
    #         print(user)
    #     except Exception as e:
    #         print("没有取到")
    #         current_app.logger.error(e)
    user=g.user

    # 右侧的新闻排行的逻辑
    news_list = []
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    # 定义一个空的字典列表，里面装的就是字典
    news_dict_li = []
    # 遍历对象列表，将对象的字典添加到字典列表中
    for news in news_list:
        news_dict_li.append(news.to_basic_dict())


    # 查询新闻数据
    news = None

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)

    if not news:
        # 报404错误，404错误统一显示页面后续再处理
        abort(404)

    # 更新新闻的点击次数
    news.clicks += 1
    is_collected = False

    # if 用户已经登录：
    #       判断用户是否已经收藏当前新闻，如果收藏
    #           is_collected = True

    if user:
        if news in user.collection_news:
            #在使用的时候会自动转换到user.collection_news.all()
            is_collected = True

    comments = []
    try:
        comments = Comment.query.filter(Comment.news_id ==news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)

    #得提前声明变量，否则：UnboundLocalError: local variable 'comment_like_ids' referenced before assignment
    comment_like_ids = []
    if g.user:
        try:

            #需求：查询当前用户在当前新闻里面都点赞啦哪些评论
            #1. 查询出当前新闻到所有评论（[COMMENT]）取到所有到评论id[1,2,3,4,5]
            comment_ids = [comment.id for comment in comments]
            #2. 再查询当前评论中哪些评论被当前用户所点赞([CommentLike]) 查询comment_id 在第一步的评论id列表内的所有数据
            #这个步骤很关键需要好好理解
            comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_(comment_ids),CommentLike.user_id == g.user.id)
            #3.取到所有被点在评论id 第2步查询出来的是一个[CommentLike] -->[3,5]
            comment_like_ids = [comment_like.comment_id for comment_like in comment_likes] #[3,5]
        except Exception as e:
            current_app.logger.error(e)


    comment_dict_li = []
    # print(comments)
    for comment in comments:
        #这个地方注意一下下，要是不把对象模型转换字典形式，则无法取到comment的属性
        #flask中将数据库模型类查询输出转变为字典格式
        #提示错误：jinja2.exceptions.UndefinedError: 'info.models.Comment object' has no attribute 'user'
        comment_dict = comment.to_dict()
        # 代表没有点赞
        comment_dict["is_like"] = False
        #判断当前遍历到的评论是否被当前用户所点赞
        if comment.id in comment_like_ids:
            comment_dict["is_like"] = True
        # print(comment_dict)
        comment_dict_li.append(comment_dict)

    is_followed = False
    # if 当前新闻有作者，并且 当前登录用户已关注过这个用户
    if news.user and user:
        # if user 是否关注过 news.user
        if news.user in user.followed:
            is_followed = True


    data = {
        "user": user.to_dict() if user else None,
        "news_dict_li": news_dict_li,
        "news":news.to_dict(),
        "is_collected":is_collected,
        "comments":comment_dict_li,
        "is_followed" : is_followed
    }
    return render_template('news/detail.html',data =data)