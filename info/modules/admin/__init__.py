# 新闻详情模块的蓝图

from flask import Blueprint

# 创建蓝图对象
from flask import redirect
from flask import request
from flask import session
from flask import url_for

admin_blu = Blueprint("admin", __name__)

from . import views_my


@admin_blu.before_request
def check_admin():
    # if 不是管理员，那么直接跳转到主页
    is_admin = session.get("is_admin", False)
    # if not is_admin and 当前访问的url不是管理登录页:
    if not is_admin and not request.url.endswith(url_for('admin.login')):
        return redirect('/')





#   后台访问权限控制 ---方法2   添加请求钩子函数
#
# @admin_blu.before_request
# def before_request():
#     #判断如果不是登录页面的请求
#     if not request.url.endswitch(url_for("admin.login")):
#         user_id = session.get("user_id")
#         is_admin = session.get("is_admin",False)
#
#         if not user_id and is_admin:
#             #判断当前是否有用户登录且是否是管理员登录
#             return redirect("/")
