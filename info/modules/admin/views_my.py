import time

from  datetime import datetime, timedelta

from flask import abort
from flask import current_app, jsonify
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from info import constants, db
from info.models import User, News, Category
from info.modules.admin import admin_blu
from info.utils.common import user_login_data
from info.utils.image_storage import storage
from info.utils.response_code import RET

# 新闻分类
@admin_blu.route('/news_type' ,methods=["GET","POST"])
def news_type():
    if request.method == 'GET':
    # 需要查找分类数据

        try:
            categories = Category.query.all()
        except Exception as e:
            return render_template("admin/news_type_my.html", errmsg='查询分类数据错误')
        categories_dict_li = []
        for category in categories:
            # 取到分类的字典
            category_dict = category.to_dict()

            categories_dict_li.append(category_dict)

        # 删除最新分离（但是我的页面没有设置“最新”分类，所以注释掉）
        # categories_dict_li.pop(0)

        data = {
            "cat"
            "egories": categories_dict_li
        }


        return render_template("admin/news_type_my.html",data =data)

    # post 请求 修改分类或者添加数据
    # 1. 取参数
    cname =request.json.get("name")
    # 如果传了cid 则表示是修改分类
    cid = request.json.get("id")

    if not cname:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    # category = []
    #有分类id 代表查询相关数据
    if cid:
        try:
            cid = int(cid)
            category = Category.query.get(cid)
        except Exception as e:
            current_app.logger.error(e)
            return  jsonify(errno = RET.PARAMERR,errmsg = "参数错误")
        if not category:
            return jsonify(errno =RET.NODATA ,errmsg = "未找到该分类数据")
        category.name = cname
    else:
        category =Category()
        category.name =cname
        category.id = cid
        db.session.add(category)

    return jsonify(errno = RET.OK ,errmsg = "OK")







# 编辑详情页
@admin_blu.route('/news_edit_detail',methods =["GET","POST"])
def news_edit_detail():
    if request.method =="GET":
        #点击查询相关的新闻数据并传入到模板中
        news_id = request.args.get("news_id")
        if not news_id:
            abort(404)

        try:
            news_id = int(news_id)
        except Exception as e:
            return render_template("admin/news_edit_detail_my.html",errmsg = '传入参数错误')

        try:
            news = News.query.get(news_id)
        except Exception as e:
            return render_template("admin/news_edit_detail_my.html",errmsg = "查询数据错误")

        if not news:
            return render_template("admin/news_edit_detail_my.html", errmsg='没有查询到该新闻')

        #需要查找分类数据

        try:
            categories = Category.query.all()
        except Exception as e:
            return render_template("admin/news_edit_detail_my.html", errmsg='查询分类数据错误')
        categories_dict_li = []
        for category in categories:
            #取到分类的字典
            category_dict = category.to_dict()
            #判断遍历到的分类是否是当前新闻的分类，如果是则设置is_selected 为True
            if category.id == news.category_id:
                category_dict["is_selected"] = True
            categories_dict_li.append(category_dict)


        # 删除最新分离（但是我的页面没有设置“最新”分类，所以注释掉）
        # categories_dict_li.pop(0)

        data={
            "news":news,
            "categories":categories_dict_li
        }


        return render_template("admin/news_edit_detail_my.html",data =data)

    # 取到POST 进来的数据    图片是ajax进行提交
    news_id = request.form.get("news_id")
    title = request.form.get("title")
    digest = request.form.get("digest")
    content = request.form.get("content")
    index_image = request.files.get("index_image")
    category_id = request.form.get("category_id")
    # 1.1 判断数据是否有值
    if not all([title, digest, content, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数有误")

    # 根据新闻id 查询新闻这个对象
    # news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno = RET.DBERR,errmsg = "查询数据错误")
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到新闻数据")

    # 1.2 尝试读取图片
    if index_image:
        try:
            index_image = index_image.read()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg="参数有误")

        # 2. 将标题图片上传到七牛
        try:
            key = storage(index_image)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.THIRDERR, errmsg="上传图片错误")
        news.index_image_url = constants.QINIU_DOMIN_PREFIX + key

    # 3. 设置相关数据
    news.title = title
    news.digest = digest
    news.content = content
    news.category_id = category_id

    return jsonify(errno = RET.OK, errmsg = "编辑成功OK")




# 新闻版式编辑
@admin_blu.route('/news_edit')
def news_edit():
    """返回待审核新闻列表"""

    page = request.args.get("p", 1)
    # 这里有点疑问 为什么不是form-------answer: 表单提交页面跳转 通过url请求数据，因此是args.get 请求数据
    keywords = request.args.get("keywords",None)

    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    news_list = []
    current_page = 1
    total_page = 1

    filters = [News.status ==0]
    # 如果关键字存在，那么就添加关键字搜索
    if keywords:
        filters.append(News.title.contains(keywords))


    try:
        # *filter 解包
        # order_by 可以根据点击量进行排序
        paginate = News.query.filter(*filters) \
            .order_by(News.create_time.desc()) \
            .paginate(page, constants.ADMIN_NEWS_PAGE_MAX_COUNT, False)

        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_list = []
    for news in news_list:
        news_dict_list.append(news.to_basic_dict())

    context = {"total_page": total_page, "current_page": current_page, "news_list": news_dict_list}

    return render_template('admin/news_edit_my.html', data=context)







@admin_blu.route('/news_review_action',methods = ["POST"])
def news_review_action():
    #1. 接收参数
    news_id = request.json.get("news_id")
    action = request.json.get("action")

    #2. 参数校验
    if not all([news_id,action]):
        return render_template(errno=RET.PARAMERR,errmsg="参数错误")
    if action not in("accept","reject"):
        return render_template(errno=RET.PARAMERR,errmsg="参数错误")

    #  查询到指定的新闻数据
    try:
        news =News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno = RET.DBERR, errmsg = "查询数据失败")

    if not news:
        return jsonify(errno = RET.NODATA, errmsg = "没有该新闻数据")

    if action == "accept":
        #代表接受
        news.status = 0
    else:
        #代表拒绝
        reason = request.json.get("reason")
        if not reason:
            return jsonify(errno = RET.PARAMERR,errmsg ="请输入拒绝理由")
        news.reason =reason
        news.status = -1

    return  jsonify(errno = RET.OK , errmsg = "OK")




@admin_blu.route('/news_review_detail_my/<int:news_id>')
def news_review_detail(news_id):
    # 获取新闻id  这里有两种方法 ：1.从url 中获取
    #1.
    # news_id = request.args.get("news_id")
    # if not news_id:
    #     return render_template('admin/news_review_detail.html', data={"errmsg": "未查询到此新闻"})
    #
    #2. 这种方式不想要进行args获取新闻，并加以判断，直接执行后面的就行

    news =None
    try:
        news =News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)

    if not news:
        # return render_template("admin/news_review_detail_my.html",errmsg = "获取不到该新闻")
        # 正确的code 方式 ，上述是错误的，要通过data来传递errmsg
        return render_template("admin/news_review_detail_my.html", data={"errmsg": "未查询到此新闻"})

    #返回数据
    data = {"news":news.to_dict()}



    return render_template('admin/news_review_detail_my.html',data = data)

@admin_blu.route('/news_review')
def news_review():
    """返回待审核新闻列表"""

    page = request.args.get("p", 1)
    # 这里有点疑问 为什么不是form-------answer: 表单提交页面跳转 通过url请求数据，因此是args.get 请求数据
    keywords = request.args.get("keywords",None)

    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    news_list = []
    current_page = 1
    total_page = 1

    filters = [News.status !=0]
    # 如果关键字存在，那么就添加关键字搜索
    if keywords:
        filters.append(News.title.contains(keywords))


    try:
        # *filter 解包
        paginate = News.query.filter(*filters) \
            .order_by(News.create_time.desc()) \
            .paginate(page, constants.ADMIN_NEWS_PAGE_MAX_COUNT, False)

        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_list = []
    for news in news_list:
        news_dict_list.append(news.to_review_dict())

    context = {"total_page": total_page, "current_page": current_page, "news_list": news_dict_list}

    return render_template('admin/news_review_my.html', data=context)




@admin_blu.route('/user_list')
def user_list():

    page = request.args.get("page",1)

    try:
        page =int(page)
    except Exception as e:
        current_app.logger.error(e)

    users = []
    current_page = 1
    total_page = 1

    try:
        paginate = User.query.filter(User.is_admin == False).order_by(User.last_login.desc()).paginate(page,constants.ADMIN_USER_PAGE_MAX_COUNT,False)
        # paginate = User.query.filter(User.is_admin == False).order_by(User.last_login.desc()).paginate(page, constants.ADMIN_USER_PAGE_MAX_COUNT, False)
        users = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    # 进行模型列表转字典列表
    news_dict_li = []
    for user in users:
        #   这里to_dict() 是没有register 和 last_login 的方法
        news_dict_li.append(user.to_admin_dict())

    data = {
        "total_page": total_page,
        "current_page": current_page,
        "users":news_dict_li,
    }

    return render_template('admin/user_list_my.html',data=data)



@admin_blu.route('/user_count')
def user_count():
    total_count = 0
    try:
        total_count =User.query.filter(User.is_admin == False).count()
    except Exception as e:
        current_app.logger.error(e)

    #月新增数
    mon_count= 0
    t = time.localtime()
    begin_mon_date_str = '%d-%02d-01' %(t.tm_year,t.tm_mon)
    # strptime  是将时间字符串转换成时间对象
    begin_mon_date = datetime.strptime(begin_mon_date_str,"%Y-%m-%d")
    try:
        mon_count = User.query.filter(User.is_admin == False,User.create_time>begin_mon_date).count()
    except Exception as e:
        current_app.logger.error(e)

    #日新增数
    day_count = 0
    begin_day_date_str = '%d-%02d-%02d' % (t.tm_year, t.tm_mon,t.tm_mday)
    begin_day_date = datetime.strptime(begin_day_date_str, "%Y-%m-%d")
    try:
        day_count = User.query.filter(User.is_admin == False, User.create_time > begin_day_date).count()
    except Exception as e:
        current_app.logger.error(e)


    #拆线图数据
    active_time = []
    active_count = []

    # 取当前这一天，然后-1,-2,-3,-4..........
    today_date_str = ('%d-%02d-%02d' % (t.tm_year, t.tm_mon, t.tm_mday))
    # 转成时间对象
    today_date = datetime.strptime(today_date_str,"%Y-%m-%d")

    for i in range(0,31):
        #取到某一天的0点0分
        begin_date = today_date -timedelta(days=i)
        # 取到下一天的0点0分
        end_date = today_date - timedelta(days=(i-1))
        count = User.query.filter(User.is_admin == False,User.last_login>=begin_date,
                                  User.last_login<end_date).count()
        active_count.append(count)
        # 传到前端的模板是一个字符串 所以要把datetime 格式转换成字符串
        active_time.append(begin_date.strftime('%Y-%m-%d'))

        active_time.sort(reverse=False)
        active_count.sort(reverse=True)

        data = {
            "total_count": total_count,
            "mon_count": mon_count,
            "day_count": day_count,
            "active_time" :active_time,
            "active_count": active_count
        }

    return render_template('admin/user_count_my.html',data =data)


@admin_blu.route('/index')
@user_login_data
def index():
    user= g.user
    return render_template('admin/index_my.html',user = user.to_dict())


@admin_blu.route("/login",methods=["GET","POST"])
def login():
    if request.method =="GET":
        user_id = session.get("user_id",None)
        is_admin = session.get("is_admin",False)
        if user_id and is_admin:
            return redirect(url_for("admin.index"))
        return  render_template("admin/login.html")

    #1.取到登录参数
    username = request.form.get("username")
    password = request.form.get("password")

    #2.判断参数
    if not all([username,password]):
        return render_template('admin/login.html',errmsg = "参数数据错误")

    #3. 查询当前用户
    try:
        user =User.query.filter(User.mobile == username , User.is_admin ==True).first()
    except Exception as e:
        current_app.logger.error(e)
        return render_template('admin/login.html',errmsg = "用户查询失败")

    if not user:
        return render_template('admin/login.html',errmsg = "没有该用户信息")

    #4. 校验密码
    if not user.check_password(password):
        return render_template('admin/login.html',errmsg = "登录密码错误")

    #保存用户登录信息
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name
    session["is_admin"] = user.is_admin

    #跳转到管理首页面
    return redirect(url_for('admin.index'))


@admin_blu.route('/logout')
def logout():
    """
    退出登录
    :return:
    """
    # pop是移除session中的数据(dict)
    # pop 会有一个返回值，如果要移除的key不存在，就返回None
    session.pop('user_id', None)
    session.pop('mobile', None)
    session.pop('nick_name', None)
    # 要清楚is_admin的值，如果不清除，先登录管理员，会保存到session，再登录普通用户，又能访问管理员页面
    session.pop('is_admin', None)

    return redirect("admin/login")