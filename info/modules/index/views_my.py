
from flask import current_app, jsonify
from flask import g
from flask import render_template
from flask import request
from flask import session

from info.models import User, News, Category
from info.utils.common import user_login_data
from info.utils.response_code import RET
from . import index_blu

@index_blu.route('/news_list')
def news_list():
    #1.获取数据
    cid = request.args.get("cid" ,"1" )
    page = request.args.get("page","1")
    per_page = request.args.get("per_page","10")
    #2.校验数据

    try:
        page  = int(page)
        cid = int(cid)
        per_page = int(per_page)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno = RET.PARAMERR ,errmsg = "参数错误")
    filters =[News.status == 0]
    # 上述代码好像还没有学习
    if cid !=1:
        filters.append(News.category_id == cid)

    #3.查询数据
    try:
        # 问题:paginate 是怎么调用和方法
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,per_page,False)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno = RET.DBERR ,errmsg = "参数查询错误")

    #取到当前页的数据
    news_model_list = paginate.items
    total_page = paginate.pages
    current_page = paginate.page

    #问题难点：将模型对象转换成字典列表
    news_dict_li = []
    for news in news_model_list:
        news_dict_li.append(news.to_basic_dict())

    data = {
        "total_page": total_page,
        "current_page": current_page,
        "news_dict_li": news_dict_li
    }
    return  jsonify(errno =RET.OK,errmsg = "ok",data =data)






@index_blu.route('/')
@user_login_data
def index():
    # user_id = session.get("user_id")
    # user = None
    # if user_id:
    #     # 尝试查询用户的模型
    #     try:
    #         user = User.query.get(user_id)
    #         print(user)
    #     except Exception as e:
    #         print("没有取到")
    #         current_app.logger.error(e)
    user = g.user
    news_list = []
    try:
        news_list =News.query.order_by(News.clicks.desc()).limit(6)
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []

    for news in news_list:
        news_dict_li.append(news.to_basic_dict())

    #获取新闻分类数据
    categories =  Category.query.all()
    # 定义列表 保存 分类数据
    categories_li = []
    for category in categories:
        categories_li.append(category.to_dict())


    data = {"user": user.to_dict() if user else None,
            "news_dict_li":news_dict_li,
            "categories":categories_li
            }
    return render_template('news/index.html', data=data)


# 在打开网页的时候，浏览器会默认去请求根路径+favicon.ico作网站标签的小图标
# send_static_file 是 flask 去查找指定的静态文件所调用的方法
@index_blu.route('/favicon.ico')
def favicon():
    return current_app.send_static_file('news/favicon.ico')
